import telegram
import json
import random
from itertools import chain


with open("words.json") as data_file:
    words = json.load(data_file)


state = {
    "game_length": 50,
    "current_word_index": 0,
    "words_list": [],
    "right_answer": -1
}


def unknown(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Ты правда думаешь, что я сделаю такое?!")


def start(bot, update):
    reply_markup = telegram.ReplyKeyboardMarkup([["/1", "/2", "/3"]])
    state["words_list"] = random.sample(words, state["game_length"])
    state["current_word_index"] = 0

    message = "Начинается новая игра!\n"
    bot.sendMessage(chat_id=update.message.chat_id, text=message, reply_markup=reply_markup)

    next_task(bot, update)


def get_task():
    answer_indices = generate_answers(state["current_word_index"])
    random.shuffle(answer_indices)

    for key, value in enumerate(answer_indices):
        if value == state["current_word_index"]:
            state["right_answer"] = key + 1

    message = "Слово " + str(state["current_word_index"] + 1) + " из " + str(state["game_length"]) + "\n"

    question = "Что значит это странное слово: "
    question += state["words_list"][state["current_word_index"]]['word'] + "?\n"
    answers = [
        "/1 " + state["words_list"][answer_indices[0]]["definition"],
        "/2 " + state["words_list"][answer_indices[1]]["definition"],
        "/3 " + state["words_list"][answer_indices[2]]["definition"]
    ]

    message += question + "\n".join(answers)

    return message


def check_answer(bot, update):
    message = update.message.from_user.first_name + ", "
    if int(update.message.text[1]) == state["right_answer"]:
        message += "правильно!\n"
    else:
        message += "неправильно, может повезет в слудующий раз...\n"

    bot.sendMessage(chat_id=update.message.chat_id, text=message)
    next_task(bot, update)


def next_task(bot, update):

    if state["current_word_index"] < state["game_length"]:
        message = get_task()
        state["current_word_index"] += 1
    else:
        message = "Игра окончена."

    bot.sendMessage(chat_id=update.message.chat_id, text=message)


def generate_answers(right_answer):
    wrong_answer_indices = list(chain(range(0, right_answer), range(right_answer + 1, state["game_length"])))
    answer_indices = random.sample(wrong_answer_indices, 2)
    answer_indices.append(right_answer)

    return answer_indices
